const electron = require('electron')

const { app, Menu, Tray,  BrowserWindow, dialog, ipcMain } = electron;
const path = require('path');
const url = require('url')
const wallpaper = require('wallpaper')
const AppConfig = require('./config')
const conf = AppConfig.conf()
const utils = require('./utils')
const _ = require('./js/lodash.min')
const favorites = require('./js/favorites')
const FlightSearch = require('./flightsearch')
const FlightProcessor = require('./flightprocessor')
const destinations = require('./js/destinations')
const WeatherProcessor = require('./weatherprocessor')

const GeoData = require('./js/geodata')


var mouse = require('osx-mouse')();
let nextrun = 0
let mouseInWindowPrevPos = false
let mouseOutWindowPrevPos = false
let flightIndex = 0 //maintain flightIndex here, as well as in index.html
let wallInterval = 0

 
/**
 * 
 * {"name":"Madras","country":"United States","countryCode":"US",
 * "airport":"City County","code":"MDJ","lat":44.666111,"lng":-121.163055,
 * "category":2,"currency":"USD","continent":"NA","locale":"en-US"}
 */


 let flightConfig = {
                    orig: '',
                    destinations: [],
                    flightLevels: [],
                    currency: ''
                  }
 
function actionMouseInWindow() {
  if (mouseInWindowPrevPos == false) {
   // console.log("Mouse in window")
    conf.windowConfig.setDescriptionStyle()
    mouseInWindowPrevPos = true
  }
}

//hover out -> go to description 
function actionMouseOutWindow() {
  if (mouseOutWindowPrevPos == false) {
    //console.log("Mouse out window")
    conf.windowConfig.setPreviousStyle()
    mouseOutWindowPrevPos = true
  }
}


mouse.on('move', function(x, y) {

    if (conf.windowConfig.mainWindow == null)
      return

    //TODO perf test
    if (x < conf.windowConfig.x || x > (conf.windowConfig.x + conf.windowConfig.width)) {
      //    if (conf.windowConfig.expanded !== conf.expandStatus.min) 
            mouseInWindowPrevPos = false
            actionMouseOutWindow()
          return
    }

    //some padding
    if (y < conf.windowConfig.y || y > conf.windowConfig.y + conf.windowConfig.height+16) {
       //  if (conf.windowConfig.expanded !== conf.expandStatus.min) 
            mouseInWindowPrevPos = false
            actionMouseOutWindow()
        return
    }

    mouseOutWindowPrevPos = false

     actionMouseInWindow()        
});


let mainWindow; 
let tray = null
 
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.


function createConfigWindow(hideVar) {
 if (conf.windowConfig.configWindow != null) {
    let configWindow = conf.windowConfig.configWindow

    utils.sendMessageWindow(configWindow, conf.appStatus)

     if (hideVar == true) {
        conf.windowConfig.configWindow.hide()
    }
    else
      conf.windowConfig.configWindow.show()
    return
 }

 configWindow = new BrowserWindow(
    { width: 470, 
      height: 500, 
      title: '',
      maximizable:false,
      backgroundColor: '#ffd21e',
      frame: false,
      resizable: true,
      vibrancy: 'medium-light'
    }
  )              
  configWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'config.html'),
    protocol: 'file:',
    slashes: true
  }))

   configWindow.on('closed', function () {
   // configWindow = null
  })

  configWindow.setMenu(null);
  conf.windowConfig.configWindow = configWindow;

  if (hideVar == true) {
    configWindow.hide()
  }

  //utils.log.debug(ip.address()) // my ip address

  utils.sendMessageWindow(configWindow, conf.appStatus)

  //configWindow.webContents.openDevTools()
}

function createTray() {
  tray = new Tray('icons/fa-plane-16.png')
  const contextMenu = Menu.buildFromTemplate([
    {label: 'Next', type: 'normal', click: () => {
        if (conf.appStatus.uiResults.length == 0) 
          return

        conf.updateTzInfo()

        flightIndex = (flightIndex + 1) % conf.appStatus.uiResults.length
           wallpaper.set(conf.appStatus.uiResults[flightIndex].wallpaper).then(() => {
            utils.sendMessageWindow(conf.windowConfig.mainWindow, 'inc', 'mainChan')

	          utils.log.debug('Setting wallpaper to ', conf.appStatus.uiResults[flightIndex].wallpaper);
         });
    }},

    {label: 'Previous', type: 'normal', click: () => {

        if (conf.appStatus.uiResults.length == 0) 
          return

        conf.updateTzInfo()
        flightIndex = flightIndex - 1
        if (flightIndex < 0)
            flightIndex = conf.appStatus.uiResults.length - 1

           wallpaper.set(conf.appStatus.uiResults[flightIndex].wallpaper).then(() => {
            utils.sendMessageWindow(conf.windowConfig.mainWindow, 'dec', 'mainChan')
	           utils.log.debug('Setting wallpaper to ', conf.appStatus.uiResults[flightIndex].wallpaper);
        });

      /*
      wallpaper.set('wallpapers/marakech.jpg').then(() => {
	        utils.log.debug('done');
        });
        */
    }},
    {label: 'Add to favourites', type: 'normal', click: () => {
       if (conf.appStatus.uiResults.length == 0)
          return
        utils.log.debug("adding to favorites ", conf.appStatus.uiResults[flightIndex].wallpaper)
        if (favorites.addWallpaper(conf.appStatus.uiResults[flightIndex].wallpaper) > 0)
          utils.sendMessageWindow(conf.windowConfig.configWindow, [conf.appStatus.uiResults[flightIndex].wallpaper, flightIndex], 'favChan')
    }},
    {label: 'Configure App', type: 'normal', click: () => {
        createConfigWindow(false)      
    }},
    {label:'', type:'separator'},
    {label: 'Quit', type: 'normal', click: () => {
      conf.saveConfig()
      app.quit()
    }}
  ])
  tray.setToolTip('Cheap Flights')
  tray.setContextMenu(contextMenu)
}

function createMainWindow (hideVar) {
  // Create the browser window.
  let width = electron.screen.getPrimaryDisplay().workArea.width
  let height = electron.screen.getPrimaryDisplay().workArea.height
  
  utils.log.debug("WxH", width, height)

  let mX = width / 2  
  let mY = 10

  //let myWidth = 145
  //let myHeight = 11
  let myWidth = conf.windowSizes.min.width
  let myHeight = conf.windowSizes.min.height

  let posX = Number(mX - myWidth/2).toFixed(0)
  //utils.log.debug("PosX: " + posX + " " + mY)

  console.log("Window x,y ", posX-1, mY)

  mainWindow = new BrowserWindow({width: myWidth, 
                height: myHeight, x: posX-1, y: mY, transparent:true, frame:false, resizable: false/*,backgroundColor: '#d2d2d2'*/})


//  utils.log.debug(conf.windowConfig)

  // and load the index.html of the app.
  mainWindow.setMenu(null);
 
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }))

   // Emitted when the window is closed.
  mainWindow.on('closed', function () {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      mainWindow = null
      conf.windowConfig.configWindow = null
  })

  if (hideVar == true)
    mainWindow.hide()
  else
    mainWindow.show()

   conf.windowConfig.x = posX-1
   conf.windowConfig.y = mY
   conf.windowConfig.mainWindow = mainWindow

   //mainWindow.webContents.openDevTools()
}

function onFirstRunFlights() {
   utils.log.debug("** onFirstRunFlights **")
   conf.windowConfig.configWindow.hide()
   let curr = utils.currencyFormat(conf.appStatus.city.countryCode, conf.appStatus.city.currency)
      
  conf.appStatus.formattedCurrency = curr[0]
  conf.appStatus.city.currency = curr[1]
         
   utils.log.debug(conf.appStatus)
   utils.sendMessageWindow(conf.windowConfig.mainWindow, conf.appStatus)
   conf.windowConfig.mainWindow.show()
   try {
     if (conf.appStatus.uiResults.length != 0)
        wallpaper.set(conf.appStatus.uiResults[0].wallpaper).then(() => {
              utils.log.debug('Setting wallpaper to ', conf.appStatus.uiResults[0].wallpaper);
        });
   }catch(err) {

   }
}

function onFirstRunGeoLocationFlights() {
   utils.log.debug("** onFirstRunGeoLocationFlights **")
   conf.appStatus.firstTime = true
  let curr = utils.currencyFormat(conf.appStatus.city.countryCode, conf.appStatus.city.currency)
      
  conf.appStatus.formattedCurrency = curr[0]
  conf.appStatus.city.currency = curr[1]
         
   utils.sendMessageWindow(conf.windowConfig.mainWindow, conf.appStatus)
   conf.windowConfig.mainWindow.show()
   try {
        if (conf.appStatus.uiResults.length != 0)
          wallpaper.set(conf.appStatus.uiResults[0].wallpaper).then(() => {
              utils.log.debug('Setting wallpaper to ', conf.appStatus.uiResults[0].wallpaper);
          });
   }catch(err) {

   }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.

//App save file: 
// ~/Library/Application\ Support/electron-quick-start/storage/appStatus.json

function findNewFlights(destinations, onFirstRun) {

	    let flights = [];

      utils.log.debug("Finding flights for ", destinations.length, " destinations ")
      FlightSearch.allFlights(flightConfig).then((flights)=> {
          utils.log.debug("All flights results ", flights)
          let normalized = FlightProcessor.normalize(flights)
          WeatherProcessor.forLocations(normalized).then((weather_items) => {
              let i = 0
              for (i=0; i < weather_items.length; i++) {
                    try {
                      if (_.isEmpty(normalized[i]))
                        continue
                      normalized[i]['temp'] = Math.round(weather_items[i].currently.temperature)
                      normalized[i]['tz'] = weather_items[i].timezone
                      normalized[i]['temp_type'] = 'c'
                      normalized[i]['wicon'] = weather_items[i].currently.icon

                      if (conf.appStatus.city.locale == "en-US" || conf.appStatus.city.locale == "en-NZ" 
                            || conf.appStatus.city.locale == "en-AU") {
                          normalized[i]['temp'] = WeatherProcessor.toFahrenheit(normalized[i]['temp'])
                          normalized[i]['temp_type'] = 'f'
                      }
                    } catch(err) {
                        //TODO
                        utils.log.debug("ERROR getting weather")
                        utils.log.debug("^^^^^", normalized)

                        if (!_.isEmpty(normalized[i])) {
                          normalized[i]['temp'] = 'N/A'
                          normalized[i]['tz'] = ''
                          normalized[i]['temp_type'] = ''
                        }
                    }
              }
              utils.log.debug("Got weather items ", weather_items)

              conf.updateResults(normalized)
              let nextrun = utils.nextSearch(conf.appStatus.ts.lastSearch)
              utils.log.debug("Next search in ", nextrun / 1000, " seconds, ",  nextrun / 3600, " hours ")
        
              setTimeout(
                () => {
                  findNewFlights(destinations)
                },
                utils.nextSearch(conf.appStatus.ts.lastSearch)
              )

              utils.log.debug("Running on first run...")
              if (onFirstRun !== undefined) {
                onFirstRun()
              }  

          }, (err) => {
              utils.log.debug("Got weather error ", err)
              let atLeastOneOk = false
              for(i=0; i < normalized.length; i++) {
                if (!_.isEmpty(normalized[i])) {
                  normalized[i]['temp'] = 'N/A'
                  normalized[i]['tz'] = ''
                  normalized[i]['temp_type'] = ''
                }
                else 
                  atLeastOneOk = true
                }
                
                conf.updateResults(normalized)
                if (onFirstRun !== undefined && atLeastOneOk == true) {
                  onFirstRun()
                } 
                let nextrun = utils.nextSearch(conf.appStatus.ts.lastSearch)
                utils.log.debug("Next search in ", nextrun / 1000, " seconds, ",  nextrun / 3600, " hours ")
    
                setTimeout(
                  () => {
                    findNewFlights(destinations)
                  },
                  nextrun
              )  

          })
           
          //TODO retain old results

      }, (err) => {
          utils.log.debug("Got nasty flight search error")
          utils.log.debug(err)
          conf.appStatus.ts.lastSearch = utils.unix_ts()
          let nextrun = utils.nextSearch(conf.appStatus.ts.lastSearch)
          utils.log.debug("Next search in ", nextrun / 1000, " seconds, ",  nextrun / 3600, " hours ")
  
          setTimeout(
             () => {
              findNewFlights(destinations)
            },
            nextrun
          )  
      })
}


function startSlideshow() {
    if (conf.appStatus.uiResults.length == 0)
      return
    
    utils.log.debug("Wallpaper slideshow set at ", conf.appStatus.freq.wall)

 // if (conf.appStatus.freq.wall >= 3600) {
    let nextWallpaperChange = utils.nextSearch(conf.appStatus.ts.lastWall, conf.appStatus.freq.wall)
    utils.log.debug("Next wallpaper change in ", nextWallpaperChange / 1000, " seconds ")

    wallInterval = setTimeout(() => {
        conf.appStatus.curwallIdx = (conf.appStatus.curwallIdx + 1) % conf.appStatus.uiResults.length
        try {
            wallpaper.set(conf.appStatus.uiResults[conf.appStatus.curwallIdx].wallpaper).then(() => {
                flightIndex = conf.appStatus.curwallIdx
                utils.sendMessageWindow(conf.windowConfig.mainWindow, 'chgDestination ' + flightIndex, 'mainChan')
                utils.log.debug('AUTOMATIC SLIDESHOW Setting wallpaper to ', conf.appStatus.uiResults[conf.appStatus.curwallIdx].wallpaper)
                conf.appStatus.ts.lastWall = utils.unix_ts()
                
                wallInterval = setTimeout(() => {
                    startSlideshow()
                }, utils.nextSearch(conf.appStatus.ts.lastWall, conf.appStatus.freq.wall))
            });
        }catch(err) {

        }
    }, utils.nextSearch(conf.appStatus.ts.lastWall, conf.appStatus.freq.wall)) 

 //}
}

function stopSlideshow() {
    clearTimeout(wallInterval)
    wallInterval = 0
}

function processCategoryCity(origin, destination) {
    //let origin = conf.appStatus.city;
	  let searchLevel = 'City';
    //code to set category 2 or 1

    let dName = (name) => {
        name = name.split(",")
        return name[0]
    }    

    let dCountry = (airportCode) => {
      try {
        let c =  _.filter(conf.cities(), {code: airportCode})
       // utils.log.debug ("&& Return ", c[0].countryCode, " for ", airportCode)
        return c[0].countryCode
      }catch(err) {
        
      }
    }

	  let all_origin = _.filter(conf.cities(), {name: origin.name, countryCode: origin.countryCode});
	  let all_destination = _.filter(conf.cities(), {name: dName(destination.name), countryCode: dCountry(destination.code)});
    if (all_destination.length > 1 && destination.category == 2) 
        searchLevel = 'Airport';


    //origin is city, destination is airport

    //For Havana, there are 2 destinations with category 2, so choose the first one ???
    //similar for others
    if (all_destination.length > 1)  {
      destination = _.find(all_destination, {category: 1});
      if (_.isEmpty(destination))
        destination = all_destination[0]
     // utils.log.debug("Find destination categ 1/2: ", destination.code)
    }

    if (all_origin.length > 1) {
      origin = _.find(all_origin, {category: 1});  
      if (_.isEmpty(origin))
        origin = all_origin[0]  
    }

    // utils.log.debug("OUT Origin ", origin)
    // utils.log.debug("OUT Dst ", destination)
    // utils.log.debug(searchLevel)

    return {orig: origin, dst: destination, level: searchLevel}
    //return [origin, destination, searchLevel]
}

function changeCityCategoryConfig() {
    let res = {}
      for (let dst=0; dst < destinations.length; dst++) {
        res = processCategoryCity(conf.appStatus.city, destinations[dst])
        flightConfig.destinations.push(res.dst)
        flightConfig.flightLevels.push(res.level)
    }

    flightConfig.orig = res.orig
    conf.appStatus.city = res.orig
    let curr = utils.currencyFormat(conf.appStatus.city.countryCode, conf.appStatus.city.currency)
    conf.appStatus.formattedCurrency = curr[0]
    conf.appStatus.city.currency = curr[1]
    flightConfig.currency = conf.appStatus.city.currency
    utils.log.debug("** changeCityCategoryConfig changed origin city to ", conf.appStatus.city.code)
}

 app.on('ready', () => {
        process.env.GOOGLE_API_KEY=conf.google_key()
        WeatherProcessor.set_api_key(conf.dark_key())

        utils.log.setLevel('debug'); //info for production
        utils.log.format = function(level, date, message) {
          return "[" + utils.dateNowMMSS(date) + "] " + message
        };

        utils.log.debug("START")
        //process.env.GOOGLE_API_KEY = 'YOUR_KEY_HERE'
        utils.log.debug(app.getLocale())

        const geolocation = require ('google-geolocation') ({
          key: 'AIzaSyAibhf7WTR2mf4TCc-2WkKLcr3Y4n2xuOA'
        });

        //for config window checkboxes
        ipcMain.on('configChan', (event, arg) => {
            if (arg == '3600' || arg == '86400' || arg == '172800' || arg == '604800') {
                conf.appStatus.freq.wall = parseInt(arg)
            }
            //must be start on startup
            else if (arg == "autostart") {
              conf.appAutoStart()
            }
            else  if (arg == "autostartoff") {
              conf.appAutoDisable()
            }
        })

        //emitted when config city changes
        ipcMain.on('configCityChan', (event, city) => {
          utils.log.debug("[][]Config city chan ", city)
          conf.results = [] //reset the old results if any
          conf.appStatus.uiResults = []
          //search for new flights
          let oldCity = conf.appStatus.city
          conf.appStatus.city = city

          changeCityCategoryConfig()
          city = conf.appStatus.city

          //possible race here?
          conf.saveConfig()

          utils.log.debug("City changed to ", city.code)
          utils.log.debug("Finding some flights now from ", city.code)
          findNewFlights(destinations, onFirstRunFlights)
        })

        //changes wallpaper when clicked from fav config window
        ipcMain.on('favChan', (event, msg) => {
            if (msg[0] == 'chgDestination') {
              flightIndex = msg[1]
                wallpaper.set(conf.appStatus.uiResults[msg[1]].wallpaper).then(() => {
                  utils.sendMessageWindow(conf.windowConfig.mainWindow, 'chgDestination ' + msg[1], 'mainChan')
                  utils.log.debug('Setting wallpaper to ', conf.appStatus.uiResults[msg[1]].wallpaper);
              });
            }
        })

        ipcMain.on('mainChan', (cevent, msg) => {
            if (msg == 'toggleWindow') {
                conf.windowConfig.toggleStyle()
            }

            //debug only
            else if (msg.indexOf("showDimensions") != -1) {
                console.log(msg)
            }
            else if (msg.indexOf("resizeHeight") != -1) {
              //console.log(msg)
              let newHeight = Math.round(parseFloat(msg.split(' ')[1])) + 55
              conf.windowConfig.height = newHeight
              conf.windowConfig.mainWindow.setSize(conf.windowSizes.desc.width, newHeight)
            }
        })

        let testMode = false
        createConfigWindow(true)
        createMainWindow(true)

        //Load config
        let cWindow = conf.windowConfig.configWindow
      
        let failedConfAttempts = 0
        let maxFailedConf = 10

        conf.load().then((appStatus) => {

          utils.log.debug("Got appstatus")
         // utils.log.debug(appStatus.city)
          conf.updateTzInfo()
          
          utils.sendMessageWindow(cWindow, appStatus.city)
          

          //Wallpaper slideshow :)

          startSlideshow()

          //start flight search here

          let nextrun = utils.nextSearch(conf.appStatus.ts.lastSearch)

          //utils.log.debug("Before conf.results ", conf.results.length)

          let alwaysGrabResults = false

          if (conf.appStatus.uiResults.length == 0) {
              utils.log.debug("Issuing a new search right now, because of 0 results.")
              findNewFlights(destinations, onFirstRunFlights)
          }

          else {
            
            //TODO load based on hash, not index, to keep same pics in case uiResults changes
            let p=0;
            
          
            let wallp = ''

            utils.log.debug("** Loading favorite wallpapers ***")

                      //populate favorite wallpapers

            for(let wallp=0; wallp < conf.appStatus.fav.length; wallp++) {
                utils.log.debug("Sending wp favorite ", conf.appStatus.fav[wallp])
                utils.sendMessageWindow(conf.windowConfig.configWindow, [conf.appStatus.fav[wallp], favorites.locateIndex(conf.appStatus.fav[wallp])], 'favChan')
            }

            utils.log.debug("Next search in ", nextrun / 1000, " seconds ", nextrun / 3600000, " hours ")
            utils.sendMessageWindow(mainWindow, conf.appStatus)
            mainWindow.show() //might not be able to be shown on time due to results loading?
          
            setTimeout(
              () => {
                findNewFlights(destinations)
              },
              nextrun
            )       
          }
        }, (err) => {
            //first time or error reading file

            //TODO remove debug stuff from storage library
            utils.log.debug(err)

            //JSON.parse -> unexpected end of json input ??
            utils.log.debug("GEOLOCATION first time")
              geolocation ({}, (err, data) => {

                  let lat = 51.5074
                  let lng = 0.8

                  if (err || testMode == true) {
                    utils.log.debug (err);
                  }
                  else {
                    lat = data.location.lat
                    lng = data.location.lng
                  }
                  closestCity = GeoData.getClosestCity(lat, lng)
                  utils.log.debug ("City: " + JSON.stringify(closestCity))

                  conf.appStatus.city = closestCity
                  changeCityCategoryConfig()
                  
                  conf.results = []
                  conf.appStatus.uiResults = []
                  conf.appStatus.freq.wall = 3600
                  conf.appStatus.ts.lastWall = utils.unix_ts() //do not change wallpaper immediately

                  utils.log.debug("Formatted currency ", conf.appStatus.formattedCurrency)
        
                  findNewFlights(destinations, onFirstRunGeoLocationFlights)  

                  utils.sendMessageWindow(cWindow, conf.appStatus)
                  conf.saveConfig()
                  cWindow.show()  
              });
        })

        createTray()  
  })

  

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  utils.log.debug("Window all closed")
  if (process.platform !== 'darwin') {
    conf.saveConfig() //only on Windows/Linux !
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  utils.log.debug("Reactivating")
  if (mainWindow === null) {
    createMainWindow()
  }
})

app.on('quit', function() {
  try {
    //utils.log.debug("Saving config")
   //conf.saveConfig()
    mouse.destroy()
  }catch(err) {

  }
})

app.dock.hide();


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
