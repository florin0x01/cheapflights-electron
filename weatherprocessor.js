const _ = require('./js/lodash.min')
const utils = require('./utils')
const DarkSky = require('dark-sky')
const forecast = new DarkSky()
 
class WeatherProcessor {
   
    static set_api_key(key) {
        this.key = key
        forecast.apiKey = this.key
    }

     /**
     * 
     * @return Promise 
     */
    static forLocation(lng, lat) {
        return forecast
                .latitude(lat)
                .longitude(lng)
                .time(utils.dateDayStr())
                .units('si')
                .exclude('minutely,hourly,alerts,flags')
                //.extend('hourly')
                .get() 
        }

    /**
     * @return Promise 
     */
    static forLocations(arr) {
        let promises = []
        let i = 0;
        for (i=0; i < arr.length; i++) {
            //check for empty flight result and return a fake promise !!
            if (_.isEmpty(arr[i])) {
                promises.push(new Promise(
                    (resolve, reject) => {
                        resolve( 
                            {
                                currently: {temperature: 'N/A', icon: 'N/A' },
                                timezone: 'N/A'
                            } )
                        }
                ))
                continue
            }
            promises.push(WeatherProcessor.forLocation(arr[i].lng, arr[i].lat))
        }
        return Promise.all(promises)
    }

    static toFahrenheit(celsius) {
        return Math.round(celsius * 1.8 + 32)
    }    
}

module.exports = WeatherProcessor
