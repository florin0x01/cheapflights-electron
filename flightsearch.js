/*

FlightPriceExample API:
http://www.momondo.com/api/3.0/Flights/FlightPriceExample?culture=en-US&Origin=NYC&Destination=LON&SearchLevel=City&ExampleType=ReturnCheapest&EarliestDepartureDate=2016-05-01&LatestDepartureDate=2016-05-11&MaxResultsCount=1&MaxAgeDays=2&Currency=USD
 
Parameters:
-          Culture – which market the price examples are from e.g. en-US, en-GB, da-DK
-          Origin – IATA code for the departure airport (e.g. CPH, LON, NYC)
-          Destination – IATA code for the destination airport (similar to Origin)
-          SearchLevel – City by default. Other option is Airport
-          ExampleType – which type of price example:
	-  		 		 ReturnCheapest – just provide the cheapest example found
	- 				 ReturnCheapestDirect – cheapest example found but with the requirement of being direct
	-				   OnewayCheapest – cheapest one way example found
-          EarliestDepartureDate – when the departure date should be at the earliest (e.g. 2016-05-11)
-          LatestDepartureDate – when the departure date should be at the latest
-          MaxResultsCount – how many price examples to ask for
-          MaxAgeDays – how old the price example max should be
-          Currency – what currency to return prices in – default is EUR

*/

const http = require('http')
const utils = require('./utils')

class FlightSearch {

	/**
	 * momondo API base url
	 */
	static APIBASE() {
		return 'http://www.momondo.com/api/3.0'
	}

	/**
	 * search oneway flights
	 * @param  {object} params [includes all the parameters that generate the search url]
	 * @return {Promise}      
	 */
	static return_anytime(params){

		let url = `${this.APIBASE()}/Flights/FlightPriceExample`+
		'?ExampleType=ReturnCheapest' +
		'&SearchLevel=' + params.searchLevel + 
		'&MaxResultsCount=' + (params.maxResultsCount || 1) +
		'&MaxAgeDays=' + (params.maxAgeDays || 10) +
		'&Culture=' + params.culture +
		'&Currency=' + params.currency +
		'&Origin=' + params.origin +
		'&Destination=' + params.destination +
		'&EarliestDepartureDate=' + params.date +
		'&LatestDepartureDate=' + params.latestdate;  //+2 luni dist de earliest

		utils.log.debug(url)

		return new Promise( (resolve, reject) => {
			if (params.origin == params.destination) {
				utils.log.debug(" ??? Can't fly from ", params.origin, " -> ", params.destination)
				return resolve({})
			}

			//$.ajax( { url: url} ).done( resolve ).fail( reject );
			http.get(url, (res) => {
				const statusCode = res.statusCode;
  				const contentType = res.headers['content-type'];

  				let error;
  				if (statusCode !== 200) {
    				error = new Error(`Request Failed.\n` +
                      `Status Code: ${statusCode}`);
  				}
				else if (!/^application\/json/.test(contentType)) {
    				error = new Error(`Invalid content-type.\n` +
                      `Expected application/json but received ${contentType}`);
  				}	
				
				if (error) {
    				utils.log.debug(error.message);
					// consume response data to free up memory
					res.resume();
					
					//TODO do not reject anything
					return resolve({})
				}

				res.setEncoding('utf8');
  				let rawData = '';
  				res.on('data', (chunk) => rawData += chunk);
  				res.on('end', () => {
    			try {
      				let parsedData = JSON.parse(rawData);
      				//utils.log.debug(parsedData);
					return resolve(parsedData)
    			} catch (e) {
      				utils.log.debug(e.message);
					return resolve({})
   	 			}
  			});
		}).on('error', (e) => {
  			utils.log.debug(`Got error: ${e.message} for ${params.destination}`);
			//reject(e.message)
			return resolve({})
		});

	}); //end promise

  }

	/**
	 * @return {Promise}s
	 */
	static allFlights(flightConfig)
	{
		let promises = []
		let i = 0
		let origCity = flightConfig.orig.code

		//let destCity = destinations[0]

		for (i=0; i < flightConfig.destinations.length; i++) {
			let destCity = flightConfig.destinations[i].code
			let level = flightConfig.flightLevels[i]
			let currencyCode = flightConfig.currency

			//if (i==1) break;
			let params = {
				culture: 'en-US',
				currency: currencyCode,
				origin: origCity,
				destination: destCity,
				searchLevel: level,
				/*exampleType: 'ReturnCheapest'*/
				maxAgeDays: 10,
				maxResultsCount: 1,
				date: utils.dateDayStr(),
				leaving: utils.dateDayStr(),
				latestdate: utils.futureDate(60) //60 days after..
			}
			promises.push(FlightSearch.return_anytime(params))
		}
		return Promise.all(promises)
	}
}

module.exports = FlightSearch