const storage = require('electron-json-storage');
const _ = require('./js/lodash.min')
const dests = require('./js/destinations')
const utils = require('./utils')
const moment = require('moment')
const AutoLaunch = require('auto-launch');


const ONEDAY = 3600 * 24
const GOOGLE_KEY = 'AIzaSyAibhf7WTR2mf4TCc-2WkKLcr3Y4n2xuOA'
const DARK_KEY = 'f1f6e067babaa031776791f38399a9ed'

class AppConfig {
    
    constructor() {
        utils.log.debug("Constructor AppConfig..")
        let me = this
        this.results = []

        this.launcher = new AutoLaunch({
            name: 'Desktop Dreams',
            path: '/Applications/DesktopDreams.app',
        });

        this.appStatus = {
            city: {},
            fav: [],
            autostart: false,
            formattedCurrency: '',
            uiResults: [], //the results without empty stuff for UI only
            curwallIdx: 0,
            ts: {
                lastSearch: 0,
                lastSearchViz: '',
                lastWall: 0
            },
            freq: {
                wall: 3600
            }
        }

        this.expandStatus = {
            min: 0,
            info : 1,
            desc: 2
        }

        this.windowSizes = {
            min: {
                width: 175,
                height: 15
            },
            info: {
                width: 175,
                height: 60
            },

            desc: {
                width: 175,
                height: 190
            }
        }

        this.windowConfig = {
            x : 0,
            y: 0,
            width: 175,
            height: 35,
            maxHeight: 300,
            mainWindow: null,
            configWindow: null,
            expanded: this.expandStatus.min,
            expandedOld: this.expandStatus.min,
            toggle: undefined,
            toggleStyle: () => {
                
                if (me.windowConfig.toggle == undefined)
                    me.windowConfig.toggle = me.windowConfig.expanded

                 me.windowConfig.toggle = (me.windowConfig.toggle + 1) % 3; //2 expanded statuses; desc mode is already avail on win hover
                
               // console.log("Toggle ", me.windowConfig.toggle)
                 me.windowConfig.setStyle()
            },

            setInfoStyle: (ignore) => {
                //console.log("SetInfo")
               if (me.windowConfig.expanded != me.expandStatus.info || ignore == true) {

                    if (ignore == undefined) {
                        me.windowConfig.expandedOld = me.windowConfig.expanded
                        me.windowConfig.expanded = me.expandStatus.info
                    }
                    else {
                        me.windowConfig.expanded = me.windowConfig.toggle
                        me.windowConfig.expandedOld = me.windowConfig.expanded
                    }

                    me.windowConfig.height = me.windowSizes.info.height

                    me.windowConfig.width = me.windowSizes.info.width
                    me.windowConfig.mainWindow.setSize(me.windowSizes.info.width, me.windowSizes.info.height, true)
                    utils.sendMessageWindow(me.windowConfig.mainWindow, 'showInfo', 'mainChan')
                }
            },

            setDescriptionStyle: (ignore) => {
               // if (ignore == true && me.windowConfig.expanded == me.windowConfig.toggle)
                 //   return

                if (me.windowConfig.expanded != me.expandStatus.desc || ignore == true) {
                    
                    if (ignore == undefined) {
                        me.windowConfig.expandedOld = me.windowConfig.expanded
                        me.windowConfig.expanded = me.expandStatus.desc
                    }
                    else {
                        me.windowConfig.expanded = me.windowConfig.toggle
                        me.windowConfig.expandedOld = me.windowConfig.expanded
                    }

                    //for larger descriptions, choose the max
                   // me.windowConfig.height = Math.max(me.windowSizes.desc.height, me.windowConfig.height)
                    
                    if (me.windowSizes.desc.height > me.windowConfig.height)
                        me.windowConfig.height = me.windowSizes.desc.height

                    //console.log("Mainwindow height to ", me.windowConfig.height)    

                    me.windowConfig.width = me.windowSizes.desc.width
                    me.windowConfig.mainWindow.setSize(me.windowSizes.desc.width, me.windowConfig.height, true)
                    utils.sendMessageWindow(me.windowConfig.mainWindow, 'showDesc', 'mainChan')
                }
            },

            setMinimizedStyle: (ignore) => {
                if (me.windowConfig.expanded != me.expandStatus.min || ignore == true) {

                    if (ignore == undefined) {
                        me.windowConfig.expandedOld = me.windowConfig.expanded
                        me.windowConfig.expanded = me.expandStatus.min
                    }
                     else {
                        me.windowConfig.expanded = me.windowConfig.toggle
                        me.windowConfig.expandedOld = me.windowConfig.expanded
                    }
    
                    me.windowConfig.height = me.windowSizes.min.height
                    me.windowConfig.width = me.windowSizes.min.width
                    me.windowConfig.mainWindow.setSize(me.windowSizes.min.width, me.windowSizes.min.height, true)
                    utils.sendMessageWindow(me.windowConfig.mainWindow, 'hideInfo', 'mainChan')
                }
            },

            setPreviousStyle: () => {
                //console.log("Prev style: ", me.windowConfig.expandedOld)
                
              //  if(me.windowConfig.expanded == me.windowConfig.expandedOld) 
                //    return
                if (me.windowConfig.expandedOld == me.expandStatus.min) {
                    me.windowConfig.setMinimizedStyle()
                }
                else if (me.windowConfig.expandedOld == me.expandStatus.info) {
                    me.windowConfig.setInfoStyle()
                }
                else if (me.windowConfig.expandedOld == me.expandStatus.desc) {
                    me.windowConfig.setDescriptionStyle()
                }
            },

            setStyle: () => {
                if (me.windowConfig.toggle == me.expandStatus.min) {
                    me.windowConfig.setMinimizedStyle(true)
                }
                else if (me.windowConfig.toggle == me.expandStatus.info) {
                    me.windowConfig.setInfoStyle(true)
                }
                else if (me.windowConfig.toggle == me.expandStatus.desc) {
                    
                    me.windowConfig.setDescriptionStyle(true)
                }
            }
        },

         utils.log.debug(this.appStatus)
    }

    google_key() {
        return GOOGLE_KEY
    }

    appAutoStart() {
//        this.launcher.enable();
        let me = this
        this.launcher.isEnabled()
        .then(function(isEnabled){
            if(isEnabled){
                return;
            }
            me.appStatus.autostart = true
            me.launcher.enable();
        })
        .catch(function(err){
            utils.log.debug("** Config App auto start failed to enable ", err)
            // handle error 
        });
    }

    appAutoDisable() {
        try {
            this.appStatus.autostart = false
            this.launcher.disable()
        }catch(err) {
            utils.log.debug("** Config app auto start failed to disable ", err)
        }
    }

    dark_key() {
        return DARK_KEY
    }

    init() {
        let me = this 
    }

     transferResultsToUI() {
        this.appStatus.uiResults = []
        for (let idx=0; idx < this.results.length; idx++) {
            if (_.isEmpty(this.results[idx])) {
                continue
            }
            this.appStatus.uiResults.push(this.results[idx])
        }
    }

     updateDurationInfo() {
        let id = 0
         utils.log.debug("updateDurationInfo")
       
        for(id=0; id < this.results.length; id++) {
            if (!_.isEmpty(this.results[id]))
                this.results[id].durTime = utils.formatDuration(this.results[id].duration)
           //  utils.log.debug(  this.results[id].durTime )
        }    
     }

     updateTzInfo() {
        let id = 0
        utils.log.debug("updateTzInfo")
        for(id=0; id < this.results.length; id++) {
            if (!_.isEmpty(this.results[id]))
                this.results[id].curTime = utils.formatTimeTz(this.results[id].tz)
            //utils.log.debug(  this.results[id].curTime )

        }           
     }
        
     onerror(err) {
        // log any uncaught errors 
        console.error(err.stack);
     }

     //updates with newest flight results. if empty results, keep old ones back and update dates
     updateResults(results) {
         utils.log.debug ("** UPDATE RESULTS **")
         this.appStatus.ts.lastSearch = utils.unix_ts()
         this.appStatus.ts.lastSearchViz = utils.dateNowMMSS()   

         if (_.isEmpty(results))
            return

          let i=0;
          let myDay = utils.dateDayStr()

          //TODO double check!
           for(i=0; i < results.length; i++) {
                let item = results[i]
                utils.log.debug ("VERIFY RESULT ", item)
                if (!_.isEmpty(item)) {
                    this.results[i] = item
                }
                else {
                    this.results[i] = {}
                    continue
                }

                //some ancient search result, will simulate depart/return dates..
                if (moment(this.results[i].go).isBefore(myDay)) {
                    this.results[i].go = myDay
                    this.results[i].back = utils.futureDate(10)
                }

                 this.results[i].durTime = utils.formatDuration(this.results[i].duration)
                 this.results[i].curTime = utils.formatTimeTz(this.results[i].tz)
            }
            this.transferResultsToUI()
     }

     normalize() {
        let me = this
          utils.log.debug("normalize()")
         // utils.log.debug(me.appStatus)

        if (Number.isInteger(me.appStatus.freq.wall) == false)
            me.appStatus.freq.wall = 3600

        if (Number.isInteger(me.appStatus.ts.lastSearch) == false)
            me.appStatus.ts.lastSearch = 0

        if (Number.isInteger(me.appStatus.ts.lastWall) == false) {
            utils.log.debug("lastwall is not integer")
            me.appStatus.ts.lastWall = 0        
        }

        if (me.appStatus.curwallIdx > dests.length-1 || me.appStatus.curwallIdx < 0) {
            utils.log.debug("lastWall; curWallIDx not ok")
            me.appStatus.curwallIdx = 0
            me.appStatus.ts.lastWall = 0
        }

        try {
            if (me.results.length > dests.length) {
                me.results = []
                me.appStatus.ts.lastSearch = 0    
                me.appStatus.ts.lastSearchViz = ''
            }
        }catch(err) {
            //no property results for beginning
            utils.log.debug("Normalize error ", err)
            me.results = []
            me.appStatus.ts.lastSearch = 0    
            me.appStatus.ts.lastSearchViz = ''
        }

        //1h,24h,48h,1w

/*
        if (me.appStatus.freq.wall != 3600 && me.appStatus.freq.wall != 86400 && me.appStatus.freq.wall != 172800 && me.appStatus.freq.wall != 604800)
        {
            me.appStatus.freq.wall = 3600
        }
        */

        if (me.appStatus.ts.lastSearch > utils.unix_ts() || me.appStatus.ts.lastSearch < 0) {
            me.appStatus.ts.lastSearch = 0
            me.appStatus.ts.lastSearchViz = ''
        }

         if (me.appStatus.ts.lastWall > utils.unix_ts() || me.appStatus.ts.lastWall < 0) {
            me.appStatus.ts.lastWall = 0
        }
     }

     load() {
         let me = this
         return new Promise((resolve, reject)=> {
               storage.get('appStatus', (err, data) => {
                    if (err) {
                           
                        reject (err)
                        return
                    }
                    
                    if (_.isEmpty(data) || data == undefined) {
                        utils.log.debug("Rejecting data on load")
                        reject (data)
                        return
                    }

                    if (_.isEmpty(data.city)) {
                        utils.log.debug("Rejecting data city on load, city empty")
                        reject (data)
                        return
                    }

                    utils.log.debug("Load, resolving data")
                    try {
                          //only used on save, but .results lives under root config obj only
                        me.appStatus.autostart = data.autostart
                        me.appStatus.city = data.city
                        me.results = data.results
                        me.appStatus.fav = data.fav
                        me.appStatus.formattedCurrency = utils.currencyFormat(data.city.countryCode, data.city.currency)[0]
                        me.appStatus.curwallIdx = data.curwallIdx
                        me.appStatus.ts.lastSearch = data.ts.lastSearch
                        me.appStatus.ts.lastWall = data.ts.lastWall
                        me.appStatus.freq.wall = data.freq.wall 
                        if ("results" in me.appStatus)
                            delete me.appStatus.results
                        if ("firstTime" in me.appStatus)
                            delete me.appStatus.firstTime    
                        me.normalize()
                        me.updateDurationInfo()
                        me.updateTzInfo()
                        me.transferResultsToUI()
                    } catch (err) {
                        me.results = []
                        me.appStatus.uiResults = []
                        utils.log.debug("[load] invalid conf format")
                        utils.log.debug(err)
                        reject (data) 
                        return   
                    }   
                    utils.log.debug("Loaded config ")
                    //utils.log.debug(me.appStatus)
                    resolve(data)
               });
         })
    }

     save() {
       let me = this
       return new Promise((resolve, reject) => {
           try {
           utils.log.debug("-->promise saving config")
           me.appStatus.formattedCurrency = ''
           if (me.appStatus.city.airport == null)
                me.appStatus.city.airport = ''

           utils.log.debug(JSON.stringify(me.appStatus))
           //utils.log.debug(me.appStatus)
           //do not save formattedCurrency, compute it on load each time
           me.appStatus.formattedCurrency = ''
           me.appStatus.uiResults = [] //delete ui results
           try {
                let idx = 0;
                for (idx=0; idx < me.results.length; idx++) {
                    if (!_.isEmpty(me.results)) {
                        delete me.results[idx].curTime
                        delete me.results[idx].durTime
                    }
                }
           }catch(err) {

           }

           me.appStatus.results = me.results
           storage.set('appStatus', me.appStatus, (err) => {
                if (err) {
                    reject (err)
                    return
                }
                utils.log.debug("-->save config, resolving")
                //utils.log.debug(me.appStatus)
                resolve (me.appStatus)    
            });
           }catch(err) {
               reject(err)
           }
       })
     }

   

     doSave(er_cb, suc_cb) {
         this.save().then((status) => {
                    if (suc_cb != undefined)
                        suc_cb(status)
                }, (err) => {
                  if (er_cb != undefined)
                    er_cb(err)    
            })
     }

      saveConfig() {
        let me = this
        utils.log.debug("**Utils config saveconfig**")
        //utils.log.debug(me.appStatus)
        const dialog= require('electron').dialog

        me.doSave((err) => {
             dialog.showMessageBox({type: "error", title: "Error", message: "Could not save app settings. Please check if you have enough space on drive."})
        })
     }


      cities() {
         if (this._cities == undefined) {
             utils.log.debug("[conf]Cities undefined")
             this._cities = require('./js/cities')
        }
//        utils.log.debug("[conf] returning cities")

         return this._cities
     }

     static conf() {
         if (this._conf == undefined || this._conf == null) {
             utils.log.debug("====== CONF UNDEF ======")
            this._conf = new AppConfig()
         }
         utils.log.debug("Ret conf")
         return this._conf   
     }
 }



module.exports = AppConfig

