const utils = require ('./utils')
const AppConfig = require('./config')
const conf = AppConfig.conf()
const cities = conf.cities()
const _ = require('./js/lodash.min')
const destinations = require('./js/destinations')

class FlightProcessor {
    static normalize(res_flights) {
        if (res_flights.length == 0)
            return []

        let xentry = null
        let arr_res = []
        let used_Desc = {}
        let myIdx = 0

        for (xentry in res_flights) {
            let entry = res_flights[xentry][0]
            utils.log.debug("FlightProcessor Got entry ", entry)
    
    try {
            myIdx++
            if (entry == undefined || _.isEmpty(entry)) {
                utils.log.debug(" <====> Entry should be ", destinations[myIdx-1].code, " adding empty entry")
                arr_res.push({})
                continue
            } 

            let price = entry.Price
            let f1 = entry.Segments[0]
            let f2 = entry.Segments[1]

            let cit = _.filter(cities, (entry) => {
                    return (entry.code == f1.CityDestinationCode)
                })

             let lat = cit[0].lat
                let lng = cit[0].lng


//  TODO push undefined because we need the whole structure of destinations
           
           
                if (entry.Segments.length != 2)
                    continue
                
                let duration = f1.DurationInMinutes
                let e_Desc = _.filter(destinations, (entry) => {
                    return (entry.code == f1.CityDestinationCode) 
                })

                let idx = 0

                if (f1.CityDestinationCode in used_Desc)
                    used_Desc[f1.CityDestinationCode]++
                else
                    used_Desc[f1.CityDestinationCode] = 0

                let index = used_Desc[f1.CityDestinationCode]
                utils.log.debug("Got index ", index, " entry code ", f1.CityDestinationCode)

                let description = e_Desc[index].description
                let name = e_Desc[index].name
                let artist = e_Desc[index].artist
                let wallpaper = e_Desc[index].wallpaper
                let obj = e_Desc[index].obj


                let result  =   { "orig": f1.CityOriginCode, "destination": f1.CityDestinationCode, "duration": duration, 
                                  "price": price, "go": f1.TravelDate, "back": f2.TravelDate, "name": name, "wallpaper": wallpaper,
                                  "description": description, "artist": artist, "objective": obj, lat: lat, lng: lng }

                utils.log.debug("FlightProcessor entry result ", result)
                 
                arr_res.push(result)
            }catch(err) {   
                utils.log.debug(err)
            }
        }
        return arr_res
    }
}

module.exports = FlightProcessor