const _ = require('./js/lodash.min')
var log = require('logger').createLogger('cheapescape.log'); // logs to a file
const moment = require('moment')
require('moment-timezone')
require("moment-duration-format");
//var log = require('logger').createLogger()

let eu_countries = {
  austria: {num: '040', alpha2: 'AT', alpha3: 'AUT', en: 'Austria', fr: 'Autriche'},
  belgium: {num: '056', alpha2: 'BE', alpha3: 'BEL', en: 'Belgium', fr: 'Belgique'},
  bulgaria: {num: '100', alpha2: 'BG', alpha3: 'BGR', en: 'Bulgaria', fr: 'Bulgarie'},
  croatia: {num: '191', alpha2: 'HR', alpha3: 'HRV', en: 'Croatia', fr: 'Croatie'},
  cyprus: {num: '196', alpha2: 'CY', alpha3: 'CYP', en: 'Cyprus', fr: 'Chypre'},
  czech_republic: {num: '203', alpha2: 'CZ', alpha3: 'CZE', en: 'Czech Republic', fr: 'République Tchèque'},
  denmark: {num: '208', alpha2: 'DK', alpha3: 'DNK', en: 'Denmark', fr: 'Danemark'},
  estonia: {num: '233', alpha2: 'EE', alpha3: 'EST', en: 'Estonia', fr: 'Estonie'},
  finland: {num: '246', alpha2: 'FI', alpha3: 'FIN', en: 'Finland', fr: 'Finlande'},
  france: {num: '250', alpha2: 'FR', alpha3: 'FRA', en: 'France', fr: 'France'},
  germany: {num: '276', alpha2: 'DE', alpha3: 'DEU', en: 'Germany', fr: 'Allemagne'},
  greece: {num: '300', alpha2: 'GR', alpha3: 'GRC', en: 'Greece', fr: 'Grèce'},
  hungary: {num: '348', alpha2: 'HU', alpha3: 'HUN', en: 'Hungary', fr: 'Hongrie'},
  ireland: {num: '372', alpha2: 'IE', alpha3: 'IRL', en: 'Ireland', fr: 'Irelande'},
  italy: {num: '380', alpha2: 'IT', alpha3: 'ITA', en: 'Italy', fr: 'Italie'},
  latvia: {num: '428', alpha2: 'LV', alpha3: 'LVA', en: 'Latvia', fr: 'Lettonie'},
  lithuania: {num: '440', alpha2: 'LT', alpha3: 'LTU', en: 'Lithuania', fr: 'Lituanie'},
  luxembourg: {num: '442', alpha2: 'LU', alpha3: 'LUX', en: 'Luxembourg', fr: 'Luxembourg'},
  malta: {num: '470', alpha2: 'MT', alpha3: 'MLT', en: 'Malta', fr: 'Malte'},
  netherlands: {num: '578', alpha2: 'NO', alpha3: 'NOR', en: 'Netherlands', fr: 'Norvège'},
  poland: {num: '616', alpha2: 'PL', alpha3: 'POL', en: 'Poland', fr: 'Pologne'},
  portugal: {num: '620', alpha2: 'PT', alpha3: 'PRT', en: 'Portugal', fr: 'Portugal'},
  romania: {num: '642', alpha2: 'RO', alpha3: 'RO', en: 'Roumania', fr: 'Roumanie'},
  slovakia: {num: '703', alpha2: 'SK', alpha3: 'SVK', en: 'Slovakia', fr: 'Slovéquie'},
  slovenia: {num: '705', alpha2: 'SI', alpha3: 'SVN', en: 'Slovenia', fr: 'Slovénie'},
  spain: {num: '724', alpha2: 'ES', alpha3: 'ESP', en: 'Spain', fr: 'Espagne'},
  sweden: {num: '752', alpha2: 'SE', alpha3: 'SWE', en: 'Sweden', fr: 'Suède'}
 /* united_kingdom: {num: '826', alpha2: 'GB', alpha3: 'GBR', en: 'United Kingdom', fr: 'Royaume-Uni'},*/
};

function currencyFormat(countryCode, currency) {
    if (countryCode == 'NZ')
        return ["$NZ", currency]
    if (countryCode == "CA")
        return ["$CA", currency]
    if (countryCode == "US") 
        return ["$", currency]
    if (countryCode == "UK" || countryCode == "GB")
        return ['\u00a3', currency]
    let results = _.filter(eu_countries, (c) => {
        return (c.alpha2 == countryCode)
     })
     if (results.length >= 1)
        return ['\u20ac', 'EUR']; //euro everywhere!

     //for other countries, return first letter
     return [currency[0], currency]
}

function unix_ts() {
    return Math.floor((new Date).getTime()/1000)
}

function formatTimeTz(tiz) {
    try {
        return moment().tz(tiz).format('HH.mm A')
    }catch(err) {
        return 'N/A'
    }
}

function formatDuration(minutes) {
    try {
        let dur = moment.duration(minutes, "minutes").format("h.mm");
        let spl = dur.split('.')
        return spl[0] + 'h' + spl[1]
    }catch(err) {
        return 'N/A'
    }
}

function dateDayStr(curDate) {
    if (typeof Date.prototype.yyyymmdd === "undefined") {    
        Date.prototype.yyyymmdd = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();
        

        return [this.getFullYear(),
                (mm>9 ? '' : '0') + mm,
                (dd>9 ? '' : '0') + dd
                ].join('-');
        };
    }

    if (curDate == undefined)
        return (new Date()).yyyymmdd()
    else
        return (new Date(curDate).yyyymmdd())
}

function dateNowMMSS(curDate) {
    if (typeof Date.prototype.yyyymmddhhss === "undefined") {    
        Date.prototype.yyyymmddhhss = function() {
        var mm = this.getMonth() + 1; // getMonth() is zero-based
        var dd = this.getDate();
        var hour = this.getHours();
        var min = this.getMinutes()
        var s = this.getSeconds()
        
        return [this.getFullYear(),
                (mm>9 ? '' : '0') + mm,
                (dd>9 ? '' : '0') + dd,
                (hour>9 ? '' : '0') + hour,
                (min>9 ? '' : '0') + min,
                (s>9 ? '' : '0') + s                
                ].join('-');
        };
    }

    if (curDate == undefined)
        return (new Date()).yyyymmddhhss()
    else
        return (curDate.yyyymmddhhss())
}

function futureDate(afterDays, dateObj) {
    if (afterDays == null)
        afterDays = 7

    let date = null
    if (dateObj == undefined)
        date = new Date()
    else
        date = new Date(dateObj)

    date.setDate(date.getDate() + afterDays);

    return dateDayStr(date)
}

function nextSearch(lastSearch, _future) {
    if (lastSearch <= 0)
        return 3000

    let futureSeconds = 0

    if (_future == undefined)
         futureSeconds = 24 * 3600 //+24 hours
    else    
        futureSeconds = _future

    let nextF = (lastSearch + futureSeconds -  unix_ts()) * 1000
    return nextF
}


function sendMessageWindow(window, msg, channel) {
  if (_.isEmpty(window)) {
    //utils.log.debug("Window empty , msg: ", msg)
    return
  }

  if (channel == undefined)
    channel = 'cityChan'


   //utils.log.debug("sendMessageWindow")
   //TODO verify this!

     //TODO avoid sending twice?!

   //workaround. mainChan messages shouldn't be sent on did-finish-load
   if (channel != 'mainChan') {
    window.webContents.on('did-finish-load', () => {
            log.debug("Event")
            //utils.log.debug(msg)
            if (!_.isEmpty(msg)) {
            //utils.log.debug("EVVTX")
            //utils.log.debug("sendMessageWindow send message [", msg, "] to ", window)
                window.webContents.send(channel, msg)
            }
        })
    }

     try {
        if (!_.isEmpty(msg))
          window.webContents.send(channel, msg)
     }catch(err) {

     }
}

module.exports = { currencyFormat, unix_ts, dateDayStr, dateNowMMSS, futureDate, nextSearch, log, sendMessageWindow, formatTimeTz, formatDuration }