const AppConfig = require('../config')
const conf = AppConfig.conf()
const utils = require('../utils')

function locateIndex(wallpaper) {
      for(let idx=0; idx < conf.appStatus.uiResults.length; idx++) {
            if (conf.appStatus.uiResults[idx].wallpaper == wallpaper)
                return idx
      }
      return -1
}

function addWallpaper(wallpaper) {
    for(let idx=0; idx < conf.appStatus.fav.length; idx++) {
        utils.log.debug("Veirfy ", conf.appStatus.fav[idx], wallpaper)
        if (conf.appStatus.fav[idx] == wallpaper) {
            utils.log.debug("Double")   
            return 0
        }
    }
            
    conf.appStatus.fav.push(wallpaper)
    return 1
}

module.exports = {locateIndex, addWallpaper}