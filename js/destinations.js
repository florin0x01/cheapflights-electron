module.exports = [
  {
    "file": "bkk",
    "code": "BKK",
    "wallpaper": "wallpapers/bkk.jpg",
    "name": "Bangkok, Thailand",
    "artist": "Sven Scheuermeier",
    "obj": "Tuk Tuk Riding",
    "description": "First time in Bangkok? Then there's no better way to savour the sights and sounds of the place they call 'The Big Mango' than a Technicolour tuk tuk."
  },
  {
    "file": "bkk2",
    "code": "BKK",
    "wallpaper": "wallpapers/bkk2.jpg",
    "name": "Bangkok, Thailand",
    "artist": "Rafa Prada",
    "obj": "Grand Palace",
    "description": "The Grand Palace of Bangkok is one of Thailand's crowning jewels. Immerse yourself in its architecture, colour and beautiful mystique."
  },
  {
    "file": "ktm",
    "code": "KTM",
    "wallpaper": "wallpapers/ktm.jpg",
    "name": "Kathmandu, Nepal",
    "artist": "Ashes Sitoula",
    "obj": "Holy Man",
    "description": ""
  },
  {
    "file": "ktm2",
    "code": "KTM",
    "wallpaper": "wallpapers/ktm2.jpg",
    "name": "Kathmandu, Nepal",
    "artist": "Ben Pauer",
    "obj": "Monkey",
    "description": ""
  },
  {
    "file": "grx",
    "code": "GRX",
    "wallpaper": "wallpapers/grx.jpg",
    "name": "Granada, Spain",
    "artist": "Victoriano Izquierdo",
    "obj": "Alhambra",
    "description": "Named after the Arabic for ‘Red Castle’, the enchanting Alhambra is part palace, part fort. Where fountains trickle and ancient spirits linger."
  },
  {
    "file": "cod",
    "code": "COD",
    "wallpaper": "wallpapers/cod.jpg",
    "name": "Wyoming, USA",
    "artist": "Todd Cravens",
    "obj": "Yellowstone National Park",
    "description": ""
  },
  {
    "file": "rek",
    "code": "REK",
    "wallpaper": "wallpapers/rek.jpg",
    "name": "Iceland",
    "artist": "Jeremy Goldberg",
    "obj": "Black Diamond Beach",
    "description": ""
  },
  {
    "file": "blr",
    "code": "BLR",
    "wallpaper": "wallpapers/blr.jpg",
    "name": "Bengaluru, India",
    "artist": "Shreyas Malavalli",
    "obj": "Bird",
    "description": ""
  },
  {
    "file": "del",
    "code": "DEL",
    "wallpaper": "wallpapers/del.jpg",
    "name": "New Delhi, India",
    "artist": "Igor Ovsyannykov",
    "obj": "Qutub Minar",
    "description": "The decorative Qutab Minar is a soaring, 73m tower built in 1193 to celebrate the Muslim rule of India. This world heritage site is a sight to behold."
  },
  {
    "file": "yto",
    "code": "YTO",
    "wallpaper": "wallpapers/yto.jpg",
    "name": "Toronto, Canada",
    "artist": "Alexandru Gogan",
    "obj": "CN Tower",
    "description": ""
  },
  {
    "file": "nbo",
    "code": "NBO",
    "wallpaper": "wallpapers/nbo.jpg",
    "name": "Kenya, Africa",
    "artist": "Jordi Fernandez",
    "obj": "Amboseli National Park",
    "description": ""
  },
  {
    "file": "fll",
    "code": "FLL",
    "wallpaper": "wallpapers/fll.jpg",
    "name": "Florida, USA",
    "artist": "Lance Asper",
    "obj": "Fort Lauderdale",
    "description": ""
  },
  {
    "file": "lax",
    "code": "LAX",
    "wallpaper": "wallpapers/lax.jpg",
    "name": "Los Angeles, California, USA",
    "artist": "Mat Weller",
    "obj": "Hollywood Sign,",
    "description": ""
  },
  {
    "file": "lax1",
    "code": "LAX",
    "wallpaper": "wallpapers/lax1.jpg",
    "name": "Los Angeles, California, USA",
    "artist": "Joe Cooke",
    "obj": "Venice Skatepark",
    "description": ""
  },
  {
    "file": "lax2",
    "code": "LAX",
    "wallpaper": "wallpapers/lax2.jpg",
    "name": "Los Angeles, California, USA",
    "artist": "Anthony Menecola",
    "obj": "Walt Disney Concert Hall",
    "description": ""
  },
  {
    "file": "ber",
    "code": "BER",
    "wallpaper": "wallpapers/ber.jpg",
    "name": "Berlin, Germany",
    "artist": "Shayne House",
    "obj": "Brandenberg Gate",
    "description": ""
  },
  {
    "file": "asr",
    "code": "ASR",
    "wallpaper": "wallpapers/asr.jpg",
    "name": "Cappadocia, Turkey",
    "artist": "Yonatan Anugerah",
    "obj": "Balloons",
    "description": ""
  },
  {
    "file": "muc",
    "code": "MUC",
    "wallpaper": "wallpapers/muc.jpg",
    "name": "Bavaria, Germany",
    "artist": "Nico Benedickt",
    "obj": "Neuschwanstein Castle",
    "description": ""
  },
  {
    "file": "psa",
    "code": "PSA",
    "wallpaper": "wallpapers/psa.jpg",
    "name": "Siena, Italy",
    "artist": "Davide Cantelli",
    "obj": "Tower of Mangia",
    "description": ""
  },
  {
    "file": "rek2",
    "code": "REK",
    "wallpaper": "wallpapers/rek2.jpg",
    "name": "Iceland",
    "artist": "Robert Lukeman",
    "obj": "Seljalandsfoss Waterfall",
    "description": ""
  },
  {
    "file": "mle",
    "code": "MLE",
    "wallpaper": "wallpapers/mle.jpg",
    "name": "Thondu, Maldives",
    "artist": "Shifaz Huthee",
    "obj": "Palms",
    "description": ""
  },
  {
    "file": "phx",
    "code": "PHX",
    "wallpaper": "wallpapers/phx.jpg",
    "name": "Arizona, USA",
    "artist": "Ashim D'Silva",
    "obj": "Antelope Canyon",
    "description": ""
  },
  {
    "file": "nyc",
    "code": "NYC",
    "wallpaper": "wallpapers/nyc.jpg",
    "name": "New York, USA",
    "artist": "Anthony Delanoix",
    "obj": "Flatiron Building",
    "description": ""
  },
  {
    "file": "cuz",
    "code": "CUZ",
    "wallpaper": "wallpapers/cuz.jpg",
    "name": "Cuzco, Peru",
    "artist": "Pedro Lastra",
    "obj": "Moray Terraces",
    "description": ""
  },
  {
    "file": "nyc2",
    "code": "NYC",
    "wallpaper": "wallpapers/nyc2.jpg",
    "name": "New York, USA",
    "artist": "Anthony Delanoix",
    "obj": "Brooklyn Bridge",
    "description": ""
  },
  {
    "file": "ams",
    "code": "AMS",
    "wallpaper": "wallpapers/ams.jpg",
    "name": "Amsterdam, Netherlands",
    "artist": "Sylwia Forysinska",
    "obj": "Tulips",
    "description": ""
  },
  {
    "file": "ams2",
    "code": "AMS",
    "wallpaper": "wallpapers/ams2.jpg",
    "name": "Amsterdam, Netherlands",
    "artist": "Vincent Versluis",
    "obj": "Windmill",
    "description": ""
  },
  {
    "file": "uln",
    "code": "ULN",
    "wallpaper": "wallpapers/uln.jpg",
    "name": "Mongolia",
    "artist": "Sarah Lachise",
    "obj": "Khongoryn Els",
    "description": ""
  },
  {
    "file": "nyc3",
    "code": "NYC",
    "wallpaper": "wallpapers/nyc3.jpg",
    "name": "New York, USA",
    "artist": "Annie Spratt",
    "obj": "Statue of Liberty",
    "description": ""
  },
  {
    "file": "okd",
    "code": "OKD",
    "wallpaper": "wallpapers/okd.jpg",
    "name": "Japan",
    "artist": "Thomas Tucker",
    "obj": "Koto Ku",
    "description": ""
  },
  {
    "file": "osl",
    "code": "OSL",
    "wallpaper": "wallpapers/osl.jpg",
    "name": "Norway",
    "artist": "Todd Tiemer",
    "obj": "Geilo",
    "description": ""
  },
  {
    "file": "edi",
    "code": "EDI",
    "wallpaper": "wallpapers/edi.jpg",
    "name": "Scotland",
    "artist": "Thomas Tucker",
    "obj": "Glenfinnan Viaduct",
    "description": ""
  },
  {
    "file": "mhd",
    "code": "MHD",
    "wallpaper": "wallpapers/mhd.jpg",
    "name": "Iran",
    "artist": "Sander Van Dijk",
    "obj": "Yazd",
    "description": ""
  },
  {
    "file": "bcn",
    "code": "BCN",
    "wallpaper": "wallpapers/bcn.jpg",
    "name": "Barcelona, Spain",
    "artist": "Alexandre Perotto",
    "obj": "Casa Milà",
    "description": ""
  },
  {
    "file": "bcn2",
    "code": "BCN",
    "wallpaper": "wallpapers/bcn2.jpg",
    "name": "Barcelona, Spain",
    "artist": "Camille Minouflet",
    "obj": "Barcelona City",
    "description": ""
  },
  {
    "file": "mhd2",
    "code": "MHD",
    "wallpaper": "wallpapers/mhd2.jpg",
    "name": "Yazd, Iran",
    "artist": "Sander Van Dijk",
    "obj": "Mosque",
    "description": ""
  },
  {
    "file": "osa",
    "code": "OSA",
    "wallpaper": "wallpapers/osa.jpg",
    "name": "Kyoto-shi, Japan",
    "artist": "Erol Ahmed",
    "obj": "Bamboo Forest",
    "description": ""
  },
  {
    "file": "dxb",
    "code": "DXB",
    "wallpaper": "wallpapers/dxb.jpg",
    "name": "Dubai",
    "artist": "Tim De Groot",
    "obj": "Desert",
    "description": ""
  },
  {
    "file": "par",
    "code": "PAR",
    "wallpaper": "wallpapers/par.jpg",
    "name": "Paris, France",
    "artist": "Noah Rosenfield",
    "obj": "Arc de Triomphe",
    "description": ""
  },
  {
    "file": "par2",
    "code": "PAR",
    "wallpaper": "wallpapers/par2.jpg",
    "name": "Paris, France",
    "artist": "John Towner",
    "obj": "Impasse Saint-Eustache",
    "description": "See why the Impasse Saint-Eustache is the most visited church in Paris, with its stunning renaissance interior and all-year round orchestral performaces."
  },
  {
    "file": "par3",
    "code": "PAR",
    "wallpaper": "wallpapers/par3.jpg",
    "name": "Paris, France",
    "artist": "Pedro Lastra",
    "obj": "Cathedrale Notre-dame de Paris",
    "description": "Take a tour among the gothic gargoyles of Notre-Dame de Paris and reward yourself with the most stunning view of the city."
  },
  {
    "file": "jro",
    "code": "JRO",
    "wallpaper": "wallpapers/jro.jpg",
    "name": "Tanzania, Africa",
    "artist": "Joel Peel",
    "obj": "Kilimanjaro",
    "description": "Kilimanjaro is the highest freestanding mountain in the world. Climb above the clouds and admire the wild landscape of Tanzania and Kenya."
  },
  {
    "file": "rep",
    "code": "REP",
    "wallpaper": "wallpapers/rep.jpg",
    "name": "Cambodia",
    "artist": "Cristian Moscoso",
    "obj": "Angkor Wat",
    "description": "See for yourself why Angkor Wat, nestled in the Cambodian countryside, is one of the seven wonders of the world."
  },
  {
    "file": "pmi",
    "code": "PMI",
    "wallpaper": "wallpapers/pmi.jpg",
    "name": "Palma de Mallorca, Spain",
    "artist": "Michael Hacker",
    "obj": "Pollenca",
    "description": "Ramble up to the top of the peaks of Pollenca in Palma de Mallorca and get stunning views of the of the ancient town below."
  },
  {
    "file": "ist",
    "code": "IST",
    "wallpaper": "wallpapers/ist.jpg",
    "name": "Turkey",
    "artist": "Daniel Burka",
    "obj": "Istanbul",
    "description": "Istanbul is a bustling metropolis packed with adventure. Explore this treasure trove of a city that sits between Europe and Asia."
  },
  {
    "file": "buh",
    "code": "BUH",
    "wallpaper": "wallpapers/buh.jpg",
    "name": "Bucharest, Romania",
    "artist": "Tuchiu Angelo",
    "obj": "Dunarea La Cazane",
    "description": "The face of king Decebals is carved into the dramatic shoreline of Dunarea La Cazane in Romania, making it the tallest stone statue in Europe."
  },
  {
    "file": "buh2",
    "code": "BUH",
    "wallpaper": "wallpapers/buh2.jpg",
    "name": "Bucharest, Romania",
    "artist": "David Marcu",
    "obj": "Ciucas Peak",
    "description": ""
  },
  {
    "file": "rom",
    "code": "ROM",
    "wallpaper": "wallpapers/rom.jpg",
    "name": "Rome,Italy",
    "artist": "Jace Grandinetti",
    "obj": "Inner City",
    "description": "Explore the cosmopolitan city of Rome and be inspired by a place that has influenced art the world over."
  },
  {
    "file": "rom2",
    "code": "ROM",
    "wallpaper": "wallpapers/rom2.jpg",
    "name": "Rome, Italy",
    "artist": "Galen Crout",
    "obj": "Knights of Malta Gate Keyhole",
    "description": ""
  },
  {
    "file": "jtr",
    "code": "JTR",
    "wallpaper": "wallpapers/jtr.jpg",
    "name": "Greece",
    "artist": "Henry Perks",
    "obj": "Santorini",
    "description": "Explore the white-washed towns of Santorini and experience the most picturesque island of Greece."
  },
  {
    "file": "zag",
    "code": "ZAG",
    "wallpaper": "wallpapers/zag.jpg",
    "name": "Croatia",
    "artist": "Pascal Habermann",
    "obj": "Plitvice Lakes",
    "description": "The Plitvice Lakes in croatia Croatia boast lush landcapes, dramatic waterwalls and hiking trails through one of Europe's oldest national parks."
  },
  {
    "file": "prg",
    "code": "PRG",
    "wallpaper": "wallpapers/prg.jpg",
    "name": "Prague",
    "artist": "Jace Grandinetti",
    "obj": "St. Vitus Cathedral",
    "description": "Wonder off the streets of Prague and into the many gothic churches for a breathtaking expiernce."
  },
  {
    "file": "prg2",
    "code": "PRG",
    "wallpaper": "wallpapers/prg2.jpg",
    "name": "Prague",
    "artist": "Fabrizio Verrecchia",
    "obj": "St. John of Nepomuk",
    "description": "Putter along the old cobble streets of Prague and get a taste of Medieval times."
  },
  {
    "file": "bud",
    "code": "BUD",
    "wallpaper": "wallpapers/bud.jpg",
    "name": "Budapest, Hungary",
    "artist": "Andrew Neel",
    "obj": "Fisherman's Bastion",
    "description": "The Fisherman's Bastion neo-classical terrace gives unprecedented panoramic views of Budapest."
  },
  /*
  {
    "file": "fsz",
    "code": "FSZ",
    "wallpaper": "wallpapers/fsz.jpg",
    "name": "Japan",
    "artist": "Hghask Ekorb",
    "obj": "Mount Fuji",
    "description": "Ascend to the top of the Mount Fuji and see the beauty that is the land of the rising sun."
  },*/
  {
    "file": "jkt",
    "code": "JKT",
    "wallpaper": "wallpapers/jkt.jpg",
    "name": "Indonesia",
    "artist": "Levi Morsy",
    "obj": "Waterfall",
    "description": "Discover Indonesia – a country made of over 17,000 stunning volcanic islands, where you can find beaches, jungles and Komodo dragons."
  },
  {
    "file": "lon",
    "code": "LON",
    "wallpaper": "wallpapers/lon.jpg",
    "name": "London, UK",
    "artist": "James Padolsey",
    "obj": "Millennium Bridge",
    "description": "One of the world's most visited cities, London is a cultural metling pot steeped in tradition, with landmarks left, right and centre."
  },
  {
    "file": "yyc",
    "code": "YYC",
    "wallpaper": "wallpapers/yyc.jpg",
    "name": "Calgary",
    "artist": "Samson",
    "obj": "Inner City",
    "description": "Get absorbed by Calgary's cosmopolitan charm, steeped in wild Western culture and the surrounding rocky mountains."
  },
  {
    "file": "yto2",
    "code": "YTO",
    "wallpaper": "wallpapers/yto2.jpg",
    "name": "Toronto, Canada",
    "artist": "Austin Lee",
    "obj": "Toronto Islands",
    "description": ""
  },
  {
    "file": "fez",
    "code": "FEZ",
    "wallpaper": "wallpapers/fez.jpg",
    "name": "Morocco",
    "artist": "Tyler Hendy",
    "obj": "Fes",
    "description": ""
  },
  {
    "file": "sfo",
    "code": "SFO",
    "wallpaper": "wallpapers/sfo.jpg",
    "name": "San Francisco, USA",
    "artist": "Ramiro Checchi",
    "obj": "Golden Gate Bridge",
    "description": "Take a walk on the weirder side with San Francisco. This quirky and unique city has everything to offer."
  },
  {
    "file": "sfo2",
    "code": "SFO",
    "wallpaper": "wallpapers/sfo2.jpg",
    "name": "San Francisco, USA",
    "artist": "Peppe Ragusa",
    "obj": "Tram Lines",
    "description": "Take a tram around the hilly city of San Francisco and let the laid back vibes rub off on you."
  },
  {
    "file": "pvg",
    "code": "PVG",
    "wallpaper": "wallpapers/pvg.jpg",
    "name": "Shanghai, China",
    "artist": "Li Yang",
    "obj": "Skyline",
    "description": "Discover Shanghai and put yourself in the heart of a mega city that is the perfect blend of East meets West."
  },
  {
    "file": "hav",
    "code": "HAV",
    "wallpaper": "wallpapers/hav.jpg",
    "name": "Havana, Cuba",
    "artist": "Eva Blue",
    "obj": "Classic Car",
    "description": "Havana is vibrant and bustling city where you can engross yourself in its culture at any of the many twisting and turning streets."
  },
  {
    "file": "zqn",
    "code": "ZQN",
    "wallpaper": "wallpapers/zqn.jpg",
    "name": "Queenstown",
    "artist": "Stephen Crowley",
    "obj": "Boat",
    "description": ""
  },
  {
    "file": "rap",
    "code": "RAP",
    "wallpaper": "wallpapers/rap.jpg",
    "name": "South Dakota, USA",
    "artist": "May",
    "obj": "Mount Rushmore",
    "description": "See the iconic sculptures of past American presidents carved into the granite of Mount Rushmore in the National Memorial."
  },
  {
    "file": "per",
    "code": "PER",
    "wallpaper": "wallpapers/per.jpg",
    "name": "Perth, Australia",
    "artist": "Jack Robinson",
    "obj": "Cape Peron",
    "description": "Surround yourself with the vast coastal beauty of Cape Peron (Perth, Australia) with a stoll along the shore or a swim in the sea."
  },
  {
    "file": "sin",
    "code": "SIN",
    "wallpaper": "wallpapers/sin.jpg",
    "name": "Singapore",
    "artist": "Annie Spratt",
    "obj": "Supertree Grove",
    "description": "Be in awe with the Supertree Grove in Singapore and feel like you’ve landed on another planet."
  },
  {
    "file": "wdh",
    "code": "WDH",
    "wallpaper": "wallpapers/wdh.jpg",
    "name": "Namibia",
    "artist": "Jonatan Pie",
    "obj": "Desert",
    "description": "Go back in time to Nambia and feel the remoteness of one of the oldest deserts in the world."
  },
  {
    "file": "sel",
    "code": "SEL",
    "wallpaper": "wallpapers/sel.jpg",
    "name": "Seoul",
    "artist": "Timothy Ries",
    "obj": "Temple",
    "description": ""
  },
  {
    "file": "dps",
    "code": "DPS",
    "wallpaper": "wallpapers/dps.jpg",
    "name": "Bali, Indonesia",
    "artist": "Sven Scheuermeier",
    "obj": "Jungle",
    "description": "Get a slice of island life in Bali with lush rice terraces, volcanic mountains and rugged coastline."
  },
  {
    "file": "sin2",
    "code": "SIN",
    "wallpaper": "wallpapers/sin2.jpg",
    "name": "Singapore",
    "artist": "Melvin Tan",
    "obj": "Buddha Tooth Relic Temple",
    "description": "Explore the stunning four-storey Buddha Tooth Relic Temple that rises over the China town district of Singapore."
  },
  {
    "file": "sin3",
    "code": "SIN",
    "wallpaper": "wallpapers/sin3.jpg",
    "name": "Singapore",
    "artist": "Alberto Montalesi",
    "obj": "S.E.A Aquarium",
    "description": "The S.E.A Aquarium in Singapore is one of the world's largest aquarium and is home to over 100,000 magical sea creatures."
  },
  {
    "file": "cns",
    "code": "CNS",
    "wallpaper": "wallpapers/cns.jpg",
    "name": "Cairns, Australia",
    "artist": "Nicholas Teoh",
    "obj": "Waterfall",
    "description": ""
  },
  {
    "file": "yvr",
    "code": "YVR",
    "wallpaper": "wallpapers/yvr.jpg",
    "name": "British Columbia, Canada",
    "artist": "Kamil Szybalski",
    "obj": "Snow Capped Peaks",
    "description": "Take a trip into the wild with British Columbia, roaming the gorgeous coastline and gazing at the mighty mountains of Canada."
  },
  {
    "file": "jrs",
    "code": "JRS",
    "wallpaper": "wallpapers/jrs.jpg",
    "name": "Jerusalem",
    "artist": "Rob Bye",
    "obj": "Holy Land",
    "description": "Take a personal pilgrimage to Jerusalem and see how hip modern culture has mixed perfectly with this biblical city."
  },
  {
    "file": "mnl",
    "code": "MNL",
    "wallpaper": "wallpapers/mnl.jpg",
    "name": "Philippines",
    "artist": "Mike Aunzo",
    "obj": "Palms at Sunset",
    "description": ""
  },
  {
    "file": "mnl2",
    "code": "MNL",
    "wallpaper": "wallpapers/mnl2.jpg",
    "name": "Philippines",
    "artist": "Jake Gaviola",
    "obj": "Tan-awan Oslob",
    "description": ""
  }
]