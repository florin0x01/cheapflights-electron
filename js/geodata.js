//paths are relative inside node but absolute for browser ??
const AppConfig = require('../config')
const conf = AppConfig.conf()
const cities = conf.cities()
const _ = require('./lodash.min')

class GeoData {
	//create a new GeoData object
	constructor() {

/*
		co(function *(){
				// yield any promise 
				var result = yield Promise.resolve(1)
			}).catch(onerror);

		co(function * () {
			
		})
		*/

		function onerror(err) {
			// log any uncaught errors 
			// co will not throw any errors you do not handle!!! 
			// HANDLE ALL YOUR ERRORS!!! 
			console.error(err.stack);
		}
		
		/*
		return new Promise( (resolve, reject) => {

			navigator.geolocation.getCurrentPosition( 

				(position) => {
					let closestCity = GeoData.getClosestCity(position.coords.latitude, position.coords.longitude)
					resolve(closestCity);
				},

				() => {
					reject()
				}

			);

		});*/


		
	}


	/**
	 * returns the closest city and airport from a point on the map
	 * @param  {float} lat 
	 * @param  {float} lng
	 * @return {object}
	 */
	static getClosestCity(lat, lng) {

		//sort the cities based on the distance from the current position
		let closestCities = _.sortBy(cities, [ (city) => { return GeoData.distance(lat, lng, city.lat, city.lng) } ] );

		let closestCity = closestCities[0];
		if (closestCity.category == 1) return closestCity;
		
						
		//check if there is another city nearby with Type == 1 (type == 1 means major city and should always be used against smaller airports)
		//no need to check against the entire array, use the first 10
		let nearbyMajorCities = _.filter(closestCities.slice(0,10), (city) => { return city.category == 1})
		if (nearbyMajorCities.length > 0) closestCity = nearbyMajorCities[0];

		return closestCity;

	}


	// get the distance in km between 2 points on the map
	static distance(lat1, lon1, lat2, lon2) {
	  var R = 6371; // Radius of the earth in km
	  var dLat = (lat2 - lat1) * Math.PI / 180;  // deg2rad below
	  var dLon = (lon2 - lon1) * Math.PI / 180;
	  var a = 
	     0.5 - Math.cos(dLat)/2 + 
	     Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * 
	     (1 - Math.cos(dLon))/2;

	  return R * 2 * Math.asin(Math.sqrt(a));
	}

}

module.exports = GeoData