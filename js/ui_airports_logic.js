const _ = require('./js/lodash.min');
//import CrossStorage from './modules/storage';
const $ = require('./js/jquery');
const AppConfig = require('./config')
const utils = require('./utils')
const conf = AppConfig.conf()
const cities = conf.cities()

let selectedCity = null;
let departureCity = null;

/**
 * debounced dropdown airport list population
 */
let onDebouncedKeyPress = _.debounce( () => {
	console.log("On debounce")

	let term = $('#airport').val().toLowerCase();

	if (term.length < 3) return $('#airports').hide();

	console.log("First city: " + cities[0].name)

	let results = _.filter(cities, (city) => {
		if (city.name.toLowerCase().search(term) != -1 || ( city.airport && city.airport.toLowerCase().search(term) != -1) ) return true;
		return false;
	});


	// first sort
	results = _.sortBy(results, (city) => {

		let term = $('#airport').val().toLowerCase();
		let index = city.name.toLowerCase().search(term);

		if (index != -1) {

			if (city.category == 1 && index == 0) return index-2;
			if (index == 0) return index-1;
			return index;
		}

		index = city.airport.toLowerCase().search(term);
		if (index == 0) return index-1;
		return index;


	});

	if (results.length == 0) return $('#airports').hide();

	//second sort to promote airports in same city (applies for cities with multiple airports)
	let firstResult = results[0];

	results = _.sortBy(results, (city) => {
		if (firstResult.name == city.name && firstResult.countryCode == city.countryCode) return -1;
		return 0;
	});

	populateDropdown(results.slice(0,3));

}, 200 );



/**
 * populates a dropdown with the top search results
 * @param  {array} results [the top 5 matches]
 */
let populateDropdown = (results) => {
	
	$('#airports').show().html('');

	for (let city of results) {
		let li = city.name
		if (city.category == 1) li+=' (All)';
		if (city.category == 2) li+=' - '+city.airport+' ('+city.code+')';

		let fullname = li+' - '+city.country;

		let r = _.upperFirst($('#airport').val().toLowerCase())
		li = li.replace( r, '<span>'+r+'</span>' )

		r = $('#airport').val().toLowerCase()
		li = li.replace(r, '<span>'+r+'</span>')

		li += ' - '+city.country;

		$('#airports').append('<li data-code="'+city.code+'" data-fullname="'+fullname+'"><div class="row">'+li+'</div></li>')
	}	
}

let fullCityDisplayName = (city) => {

	let fullname = city.name
	console.log("FullnameL " + city.name)
	if (city.category == 1) fullname+=' (All)';
	if (city.category == 2) fullname+=' - '+city.airport+' ('+city.code+')';

	fullname += ' - '+city.country;

	return fullname;

}

$("#airport").on('keydown', onDebouncedKeyPress)

$('#airports').on('click', 'li', (el) => {
	selectedCity = $(el.target).closest('li').attr('data-code');
	$('#airport').val( $(el.target).closest('li').attr('data-fullname') );
	$('#airports').hide();
})

$('#edit-save').on('click', el => {
	if ( $(el.target).text() == 'EDIT' ) {
		selectedCity = null;
		$(el.target).text('SAVE');
		$('#airport').prop('disabled', false).select().focus();

		return;
	}

	if ( $(el.target).text() == 'SAVE' ) {

		$(el.target).text('EDIT');
		$('#airport').prop('disabled', true)

		if (!selectedCity && departureCity) {
			return $('#airport').val( fullCityDisplayName(departureCity) );
		}
		if (!selectedCity && !departureCity) return;

		departureCity = _.find(cities, {code: selectedCity});
		
		//save config

		require('electron').ipcRenderer.send('configCityChan', departureCity)

		//conf.appStatus.city = departureCity
		//utils.saveConfig()
	}

});


